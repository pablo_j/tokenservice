Hi, my name is Pablo J. Urbano Santos, and this is my attempt in

# Addison Global Backend Technical Assesement

I have tried to meet all requirements and also the preferences on languages and frameworks.

To check the functionality, run:

```./sbt test```

This can take some time because of the delays asked in the requirements. The tests covers all exercises, including the REST route.

If you call

```./sbt run```

A web server will start in http://localhost:8080 . Sorry, for simplicity I did not make that configurable. Once started you can test the route a http://localhost:8080/token .

**Note**: After some updates, sbt ceased to work on my Debian machine. I found this script worked around the issue:

```bash
export TERM=xterm-color
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

./sbt -java-home $JAVA_HOME "$@"
```


This is how I have addressed each of the proposed exercises.

## 1. Service Trait / Interface

I implemented both interfaces in com.gemita.tokenService.SyncTokenService and com.gemita.tokenService.AsyncTokenService.

To be sure those met the requirements, I implemented concrete classes in com.gemita.tokenService.example.ExampleSyncTokenService and com.gemita.tokenService.ExampleAsyncTokenService. Those are the tested classes in the test suite.

## 2. Service Implementation

SimpleAsyncTokenService is implemented in com.gemita.tokenService.actor.ActorSimpleAsyncTokenService, **which is not an actor**, but all it does is asking the TokenServiceActor, and check the returned result. This is how I meet the requirement of both implementing the ActorSimpleAsyncTokenService trait and encapsulate the logic in an actor.

So, ActorSimpleAsyncTokenService asks TokenServiceActor to get the token from the credentials.

TokenServiceActor asks for authorization to AuthActor and uses the returned User to ask for a UserToken to TokenActor.

If an Exception is generated at any point, it is sent to the original caller, until it is picked by ActorSimpleAsyncTokenService and is thrown by it.

None of this operations block any actor.

## 3. REST API

The API is minimal, as it only provides one route.

### POST /token

#### URL Params
None

#### Data Params

```{"username": "<username>", "password": "<password>"}```

#### Success Response

**Code:** 201
**Content:** ```{"token":"<token>"}```

#### Error Response

**Code:** 401
**Content:** Unable to authorize user

OR

**Code:** 500
**Content:** Unable to generate token

#### Comments

I decided to use the POST method because in a real application, the call will create a resource, the returned token that will be used to authenticate future calls. Following that logic, the 201 status (Created) is returned on success.

I returned 401 (Unauthorized) when the username does not match the password in upper cases, because I think that behaviour is used to mimic an authentication.

But I returned 500 (Internal Server Error) when the username starts with 'A', because I suppose that behaviour is intended to mock a server error, to see how it is is handled in the exercise.

