package com.gemita.tokenService.example

import akka.actor.{ Actor, Props, ActorSystem }
import org.scalatest._
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await
import java.text.SimpleDateFormat
import java.util.Date
import com.gemita.tokenService.{ Credentials, UserToken }

class ExampleAsyncTokenServiceSpec
    extends FlatSpec
    with Matchers{


    "An ExampleAsyncTokenService" should "get a future that gets a token from "+
        "the Credentials with the username as token" in {

        val credentials = Credentials("user", "password")
        implicit val timeout: Timeout = Timeout(11 seconds)

        val future = ExampleAsyncTokenService.requestToken(credentials)
        val token = Await.result(future, timeout.duration)
            .asInstanceOf[UserToken]

        token.token should === (credentials.username)

    }

} 