package com.gemita.tokenService.example

import akka.actor.{ Actor, Props, ActorSystem }
import org.scalatest._
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await
import java.text.SimpleDateFormat
import java.util.Date
import com.gemita.tokenService.{ Credentials, UserToken }

class ExampleSyncTokenServiceSpec
    extends FlatSpec
    with Matchers{


    "An ExampleSyncTokenService" should "get a token from the Credentials "+
        "with the username as token" in {

        var credentials = Credentials("user", "password")
        val token = ExampleSyncTokenService.requestToken(credentials)

        token.token should === (credentials.username)

    }

} 