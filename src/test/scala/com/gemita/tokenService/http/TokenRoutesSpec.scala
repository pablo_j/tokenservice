package com.gemita.tokenService.http

import scala.concurrent.duration._
import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.testkit.RouteTestTimeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }

import com.gemita.tokenService.{Credentials, UserToken}
import com.gemita.tokenService.actor.{ActorSimpleAsyncTokenService, TokenServiceActor}

class TokenRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
    with TokenRoutes {


    val tsa: ActorRef = system.actorOf(TokenServiceActor.props, "tokenServiceActor")
    override val asats = new ActorSimpleAsyncTokenService(tsa)

    lazy val routes = tokenRoutes

    "TokenRoutes" should {
        "get a token when the username matches the password in upper case and "+
        "username doesn't start with 'A' (POST /token)" in {

            val credentials = Credentials("user", "USER")
            val credEntity = Marshal(credentials).to[MessageEntity].futureValue

            val request = Post("/token").withEntity(credEntity)
            implicit val timeout = RouteTestTimeout(11.seconds)
            request ~> routes ~> check {
                status should === (StatusCodes.Created)
                contentType should === (ContentTypes.`application/json`)
            }

        }
    }

    "TokenRoutes" should {
        "return Unauthorized status when the username doesn't match the "+
        "password in upper cases" in {

            val credentials = Credentials("user", "notamatch")
            val credEntity = Marshal(credentials).to[MessageEntity].futureValue

            val request = Post("/token").withEntity(credEntity)
            implicit val timeout = RouteTestTimeout(11.seconds)
            request ~> routes ~> check {
                status should === (StatusCodes.Unauthorized)
            }

        }
    }

    "TokenRoutes" should {
        "return Internal Server Error status when the username starts with 'A'" in {

            val credentials = Credentials("user", "notamatch")
            val credEntity = Marshal(credentials).to[MessageEntity].futureValue

            val request = Post("/token").withEntity(credEntity)
            implicit val timeout = RouteTestTimeout(11.seconds)
            request ~> routes ~> check {
                status should === (StatusCodes.Unauthorized)
            }

        }
    }



}