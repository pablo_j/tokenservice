package com.gemita.tokenService.actor

import akka.actor.{ Actor, Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await
import java.text.SimpleDateFormat
import java.util.Date
import com.gemita.tokenService.{ User, UserToken }
import com.gemita.tokenService.exception.UnableToGenerateTokenException

class TokenActorSpec(_system: ActorSystem)
    extends TestKit(_system)
    with Matchers
    with FlatSpecLike
    with BeforeAndAfterAll {

    def this() = this(ActorSystem("TokenActorSpec"))

    override def afterAll: Unit = {
        shutdown(system)
    }

    "A TokenActor" should "return a UserToken which token will be the "+
        "concatenation of the userId and the current date time in UTC: "+
        "yyyy-MM-dd'T'HH:mm:ssZ" in {

        val system = ActorSystem()
        var tokenActor = system.actorOf(TokenActor.props())
        val user = User("user")

        implicit val timeout: Timeout = Timeout(6 seconds)

        val fToken = tokenActor ? TokenServiceActor.RequestToken(user)
        val userToken = Await.result(fToken, timeout.duration).asInstanceOf[UserToken]
        val tokenParts = userToken.token.split("_")

        tokenParts(0) should === (user.userId)

        val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        assert(format.parse(tokenParts(1)).isInstanceOf[Date])
    }


    it should "send an UnableToGenerateTokenException if the userId starts with 'A'" in {

        val system = ActorSystem()
        var tokenActor = system.actorOf(TokenActor.props())
        val user = User("Auser")

        implicit val timeout: Timeout = Timeout(6 seconds)

        an [UnableToGenerateTokenException] should be thrownBy {
            val fToken = tokenActor ? TokenServiceActor.RequestToken(user)
            val ex = Await.result(fToken, timeout.duration).asInstanceOf[UnableToGenerateTokenException]
            throw ex
        }

    }

}