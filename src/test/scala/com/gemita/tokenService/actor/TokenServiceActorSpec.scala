package com.gemita.tokenService.actor

import akka.actor.{ Actor, Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await
import java.text.SimpleDateFormat
import java.util.Date
import com.gemita.tokenService.{ Credentials, UserToken }
import com.gemita.tokenService.exception._

class TokenServiceActorSpec(_system: ActorSystem)
    extends TestKit(_system)
    with Matchers
    with FlatSpecLike
    with BeforeAndAfterAll {

    def this() = this(ActorSystem("TokenServiceActorSpec"))

    override def afterAll: Unit = {
        shutdown(system)
    }

    "A TokenServiceActor" should "get a token from the Credentials if the "+
        "password matches the username in upper case and the username doesn't "+
        "start with 'A'" in {

        val system = ActorSystem()
        var tsa = system.actorOf(TokenServiceActor.props())
        val credentials = Credentials("user", "USER")

        implicit val timeout: Timeout = Timeout(11 seconds)

        val future = tsa ? ActorSimpleAsyncTokenService.RequestToken(credentials)
        val userToken = Await.result(future, timeout.duration).
            asInstanceOf[UserToken]
        val tokenParts = userToken.token.split("_")

        tokenParts(0) should === (credentials.username)

        val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        assert(format.parse(tokenParts(1)).isInstanceOf[Date])

    }

    it should "send an AuthException when the provided username does not match"+
        "username in upper case" in {

        val system = ActorSystem()
        var tsa = system.actorOf(TokenServiceActor.props())
        val credentials = Credentials("user", "notamatch")

        implicit val timeout: Timeout = Timeout(11 seconds)

        an [AuthException] should be thrownBy {
            val future = tsa ? ActorSimpleAsyncTokenService.RequestToken(credentials)
            val ex = Await.result(future, timeout.duration).
                asInstanceOf[AuthException]
            throw ex
        }
    }

    it should "send an UnableToGenerateTokenException when the provided "+
        "username starts with A" in {

        val system = ActorSystem()
        var tsa = system.actorOf(TokenServiceActor.props())
        val credentials = Credentials("Auser", "AUSER")

        implicit val timeout: Timeout = Timeout(11 seconds)

        an [UnableToGenerateTokenException] should be thrownBy {
            val future = tsa ? ActorSimpleAsyncTokenService.RequestToken(credentials)
            val ex = Await.result(future, timeout.duration).
                asInstanceOf[UnableToGenerateTokenException]
            throw ex
        }
    }

} 