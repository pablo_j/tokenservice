package com.gemita.tokenService.actor

import akka.actor.{ Actor, Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await
import com.gemita.tokenService.{ Credentials, User }
import com.gemita.tokenService.exception.AuthException

class AuthActorSpec(_system: ActorSystem)
    extends TestKit(_system)
    with Matchers
    with FlatSpecLike
    with BeforeAndAfterAll {

    def this() = this(ActorSystem("AuthActorSpec"))

    override def afterAll: Unit = {
        shutdown(system)
    }

    "An AuthActor" should "validate the Credentials and return an instance of "+
        "an User with the provided userId as username if the password matches "+
        "the username in upper case" in {

        val system = ActorSystem()
        var authActor = system.actorOf(AuthActor.props())
        val credentials = Credentials("user", "USER")

        implicit val timeout: Timeout = Timeout(6 seconds)

        val fAuth = authActor ? TokenServiceActor.Authenticate(credentials)
        val user = Await.result(fAuth, timeout.duration).asInstanceOf[User]

        user.userId should === (credentials.username)

    }

    it should "send an AuthException when the provided username does not match"+
        "username in upper case" in {

        val system = ActorSystem()
        var authActor = system.actorOf(AuthActor.props())
        val credentials = Credentials("user", "notamatch")

        implicit val timeout: Timeout = Timeout(6 seconds)

        an [AuthException] should be thrownBy {
            val fAuth = authActor ? TokenServiceActor.Authenticate(credentials)
            val ex = Await.result(fAuth, timeout.duration).asInstanceOf[AuthException]
            throw ex
        }

    }
} 