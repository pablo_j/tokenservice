package com.gemita.tokenService.actor

import akka.actor.{ Actor, Props, ActorSystem }
import org.scalatest.{ BeforeAndAfterAll, FlatSpecLike, Matchers }
import akka.testkit.{ ImplicitSender, TestKit }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.Await
import java.text.SimpleDateFormat
import java.util.Date

import com.gemita.tokenService.{ SimpleAsyncTokenService, Credentials, UserToken }
import com.gemita.tokenService.exception._

class ActorSimpleAsyncTokenServiceSpec(_system: ActorSystem)
    extends TestKit(_system)
    with Matchers
    with FlatSpecLike
    with BeforeAndAfterAll {

    def this() = this(ActorSystem("ActorSimpleAsyncTokenServiceSpec"))

    override def afterAll: Unit = {
        shutdown(system)
    }

    "An ActorSimpleAsyncTokenService" should "get a token from the Credentials "+
        "if the password matches the username in upper case and the username "+
        "doesn't start with 'A'" in {

        val system = ActorSystem()
        val actor = system.actorOf(TokenServiceActor.props)
        val asats = new ActorSimpleAsyncTokenService(actor)
        val credentials = Credentials("user", "USER")
        implicit val timeout: Timeout = Timeout(11 seconds)

        val future = asats.requestToken(credentials)
        val userToken = Await.result(future, timeout.duration).
            asInstanceOf[UserToken]
        val tokenParts = userToken.token.split("_")

        tokenParts(0) should === (credentials.username)

        val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        assert(format.parse(tokenParts(1)).isInstanceOf[Date])

    }


    it should "throw an AuthException when the provided username does not match"+
        "username in upper case" in {

        val system = ActorSystem()
        val actor = system.actorOf(TokenServiceActor.props)
        val asats = new ActorSimpleAsyncTokenService(actor)
        val credentials = Credentials("user", "notamatch")
        implicit val timeout: Timeout = Timeout(11 seconds)

        an [AuthException] should be thrownBy {
            val future = asats.requestToken(credentials)
            val userToken = Await.result(future, timeout.duration).
                asInstanceOf[UserToken]
        }
    }

    it should "throw an UnableToGenerateTokenException when "+
        "the provided username starts with A" in {

        val system = ActorSystem()
        val actor = system.actorOf(TokenServiceActor.props)
        val asats = new ActorSimpleAsyncTokenService(actor)
        val credentials = Credentials("Auser", "AUSER")
        implicit val timeout: Timeout = Timeout(11 seconds)

        an [UnableToGenerateTokenException] should be thrownBy {
            val future = asats.requestToken(credentials)
            val userToken = Await.result(future, timeout.duration).
                asInstanceOf[UserToken]
        }
    }

} 