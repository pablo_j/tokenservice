package com.gemita.tokenService

trait SyncTokenService {
    protected def authenticate(credentials: Credentials): User
    protected def issueToken(user: User): UserToken

    def requestToken(credentials: Credentials): UserToken = {
        var user = authenticate(credentials)
        issueToken(user)
    }
}