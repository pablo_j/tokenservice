package com.gemita.tokenService.http

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.server.ExceptionHandler
import akka.util.Timeout
import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.StatusCodes.{ Unauthorized, InternalServerError, Created}
import akka.pattern.ask
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import com.gemita.tokenService.actor.TokenServiceActor
import com.gemita.tokenService.{ Credentials, UserToken }
import com.gemita.tokenService.actor.ActorSimpleAsyncTokenService
import com.gemita.tokenService.exception._


trait TokenRoutes extends JsonSupport {
    implicit def system: ActorSystem
    implicit lazy val timeout = Timeout(11.seconds)
    val exHandler: ExceptionHandler = ExceptionHandler {
        case ex: AuthException => {
            complete(HttpResponse(Unauthorized, entity = ex.getMessage))
        }
        case ex: UnableToGenerateTokenException => {
            complete(HttpResponse(InternalServerError, entity = ex.getMessage))
        }
    }

    def asats: ActorSimpleAsyncTokenService

    lazy val tokenRoutes: Route =
        handleExceptions(exHandler){
            pathPrefix("token") {
                post {
                    entity(as[Credentials]) { credentials =>
                        val token = asats.requestToken(credentials)
                        complete(Created, token)
                    }
                }
            }
        }
}