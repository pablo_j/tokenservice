package com.gemita.tokenService.http

//#json-support
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

import com.gemita.tokenService.{ Credentials, UserToken }

trait JsonSupport extends SprayJsonSupport {
    // import the default encoders for primitive types (Int, String, Lists etc)
    import DefaultJsonProtocol._

    implicit val credentialsJsonFormat = jsonFormat2(Credentials)
    implicit val userTokenJsonFormat = jsonFormat1(UserToken)

}
//#json-support