package com.gemita.tokenService.example

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import com.gemita.tokenService.{ Credentials, User, UserToken, AsyncTokenService }

object ExampleAsyncTokenService extends AsyncTokenService {
    protected def authenticate(credentials: Credentials): Future[User] = Future {
        Thread.sleep(3000)
        User(credentials.username)
    }

    protected def issueToken(user: User): Future[UserToken] = Future {
        Thread.sleep(3000)
        UserToken(user.userId)
    }

}