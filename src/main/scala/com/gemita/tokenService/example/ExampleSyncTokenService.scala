package com.gemita.tokenService.example
import com.gemita.tokenService.{ Credentials, User, UserToken, SyncTokenService }

object ExampleSyncTokenService extends SyncTokenService {
    protected def authenticate(credentials: Credentials): User = {
        User(credentials.username)
    }

    protected def issueToken(user: User): UserToken = {
        UserToken(user.userId)
    }

}