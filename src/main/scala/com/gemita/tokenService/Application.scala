package com.gemita.tokenService

import com.gemita.tokenService.http.TokenRoutes
import akka.stream.ActorMaterializer
import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import com.gemita.tokenService.actor.TokenServiceActor
import com.gemita.tokenService.actor.ActorSimpleAsyncTokenService

object Application extends App with TokenRoutes {

    implicit val system: ActorSystem = ActorSystem("tokenHttpServer")
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val tsa: ActorRef = system.actorOf(TokenServiceActor.props, "tokenServiceActor")
    val asats = new ActorSimpleAsyncTokenService(tsa)

    lazy val routes: Route = tokenRoutes

    Http().bindAndHandle(routes, "localhost", 8080)

    println(s"Server online at http://localhost:8080/")

    Await.result(system.whenTerminated, Duration.Inf)


}