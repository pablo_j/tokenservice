package com.gemita.tokenService.exception

case class UnableToGenerateTokenException(cause: Throwable = None.orNull)
    extends Exception("Unable to generate token", cause) 