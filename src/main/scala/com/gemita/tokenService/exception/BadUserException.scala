package com.gemita.tokenService.exception
import com.gemita.tokenService.User

case class BadUserException(user: User,
    cause: Throwable = None.orNull)
    extends Exception(s"Bad user $user", cause) 