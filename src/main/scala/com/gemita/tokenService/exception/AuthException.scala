package com.gemita.tokenService.exception

case class AuthException(cause: Throwable = None.orNull)
    extends Exception("Unable to authorize user", cause) 