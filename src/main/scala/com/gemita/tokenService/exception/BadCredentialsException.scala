package com.gemita.tokenService.exception
import com.gemita.tokenService.Credentials

case class BadCredentialsException(credentials: Credentials,
    cause: Throwable = None.orNull)
    extends Exception(s"Invalid credentials $credentials", cause) 