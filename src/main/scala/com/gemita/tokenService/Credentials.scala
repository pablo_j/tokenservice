package com.gemita.tokenService

case class Credentials(username: String, password: String)