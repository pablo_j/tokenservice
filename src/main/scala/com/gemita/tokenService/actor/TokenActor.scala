package com.gemita.tokenService.actor

import akka.actor.{ Actor, ActorLogging }
import akka.actor.Props
import java.util.{ Date, TimeZone }
import java.text.SimpleDateFormat
import scala.util.{ Random, Success, Failure }
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import com.gemita.tokenService.{ User, UserToken }
import com.gemita.tokenService.exception.{ BadUserException, UnableToGenerateTokenException }

object TokenActor {
    def props(): Props = Props(new TokenActor)
}

class TokenActor extends Actor with ActorLogging {

    override def receive: Receive = {
        case TokenServiceActor.RequestToken(user: User) => {
            val origin = sender
            val future = Future {
                Thread.sleep(Random.nextInt(5000))

                if(user.userId(0) == 'A') throw new BadUserException(user)

                val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                format.setTimeZone(TimeZone.getTimeZone("UTC"))
                val now = new Date()
                val token = user.userId + '_' + format.format(now)

                UserToken(token)
            }

            future onComplete {
                case Success(token: UserToken) => {
                    origin ! token
                }

                case Failure(t: Throwable) => {
                    origin ! UnableToGenerateTokenException(t)
                }
            }

        }

    }
}