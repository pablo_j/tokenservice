package com.gemita.tokenService.actor

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Success
import scala.util.Failure
import com.gemita.tokenService.{ Credentials, User, UserToken }

object TokenServiceActor {
    case class Authenticate(credentials: Credentials)
    case class RequestToken(user: User)

    def props(): Props = Props(new TokenServiceActor)
}

class TokenServiceActor extends Actor with ActorLogging {

    var authActor  = context.actorOf(AuthActor.props())
    var tokenActor = context.actorOf(TokenActor.props())

    override def receive: Receive = {

        case ActorSimpleAsyncTokenService.RequestToken(credentials: Credentials) => {
            val origin = sender // To be able to send response inside onComplete
            implicit val timeout: Timeout = Timeout(6 seconds)

            val fAuth = authActor ? TokenServiceActor.Authenticate(credentials)


            fAuth onComplete {

                case Success(user: User) => {
                    val fToken = tokenActor ? TokenServiceActor.RequestToken(user)

                    fToken onComplete {
                        case Success(token: UserToken) => origin ! token
                        case Success(ex: Exception) => {
                            log.error("ERROR: {}", ex)
                            origin ! ex
                        }

                    }


                }

                case Success(ex: Exception) => {
                    log.error("ERROR: {}", ex)
                    origin ! ex
                }

            }


        }


    }



}