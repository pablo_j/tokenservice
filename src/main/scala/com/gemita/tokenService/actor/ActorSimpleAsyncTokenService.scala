package com.gemita.tokenService.actor

import scala.util.Success
import scala.util.Failure
import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.ActorSystem
import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global

import com.gemita.tokenService.{ SimpleAsyncTokenService, Credentials, UserToken }


/**
 * Messages that the class will send to the actors system
 */
object ActorSimpleAsyncTokenService {
    case class RequestToken(credentials: Credentials)
    case object Shutdown
}


/*
 * This class isn't an actor, but it delegates all its logic to an actor.
 *
 * This is how we comply with the requirement that the trait logic is encapsulated
 * in an actor.
 *
 */
class ActorSimpleAsyncTokenService(tsac: ActorRef) extends SimpleAsyncTokenService {
    val tsa = tsac

    def requestToken(credentials: Credentials): Future[UserToken] = {
        implicit val timeout: Timeout = Timeout(11 seconds)
        val future = tsa ? ActorSimpleAsyncTokenService.RequestToken(credentials)

        val fToken = Future[UserToken] {
            val result = Await.result(future, timeout.duration)

            if( result.isInstanceOf[Exception] ) throw result.asInstanceOf[Exception]
            result.asInstanceOf[UserToken]
        }

        fToken.mapTo[UserToken]
    }

}