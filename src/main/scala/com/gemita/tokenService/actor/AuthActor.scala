package com.gemita.tokenService.actor

import akka.actor.{ Actor, ActorLogging }
import akka.actor.Props
import scala.util.{ Random, Success, Failure }
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import com.gemita.tokenService.{ Credentials, User }
import com.gemita.tokenService.exception.{ BadCredentialsException, AuthException }

object AuthActor {
    case class Validated(user: User)
    case class Failed()

    def props(): Props = Props(new AuthActor)
}

class AuthActor extends Actor with ActorLogging {

    override def receive: Receive = {
        case TokenServiceActor.Authenticate(credentials: Credentials) => {
            val origin = sender
            val future = Future {
                Thread.sleep(Random.nextInt(5001))

                if (credentials.username.toUpperCase() == credentials.password) {
                    User(credentials.username)
                } else {
                    log.error("Bad credentials {} {}", credentials.username.toUpperCase(), credentials.password)
                    throw new BadCredentialsException(credentials)
                }

            }

            future onComplete {
                case Success(user: User) => {
                    origin ! user
                }

                case Failure(t: Throwable) => {
                    origin ! new AuthException(t)
                }
            }

        }
    }
}