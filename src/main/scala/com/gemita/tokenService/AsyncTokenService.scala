package com.gemita.tokenService

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.Await

trait AsyncTokenService {
    protected def authenticate(credentials: Credentials): Future[User]
    protected def issueToken(user: User): Future[UserToken]

    def requestToken(credentials: Credentials): Future[UserToken] = Future {
        val user = Await.result(authenticate(credentials), Duration.Inf)
        Await.result(issueToken(user), Duration.Inf)
    }

}