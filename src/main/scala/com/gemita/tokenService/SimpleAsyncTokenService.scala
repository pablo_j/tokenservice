package com.gemita.tokenService

import scala.concurrent.Future

trait SimpleAsyncTokenService {
    def requestToken(credentials: Credentials): Future[UserToken]
}